// Создайте по 2 enum с разным типом RawValue

enum Seasons: String {
	case Winter = "Зима"
	case Spring = "Весна"
	case Summer = "Лето"
	case Autumn = "Осень"
}

enum Numbers: Int {
	case One = 1
	case Two = 2
	case Tree = 3
}

// Создайте несколько enum для заполнения полей стркутуры - анкета сотрудника: enum пол, enum категория возраста, enum стаж

enum Sex {
	case Male
	case Female
}
enum AgeCategory {
	case Young
	case Middle
	case Old
}
enum Experience {
	case Intern
	case Junior
	case Middle
	case Senior
}

// Создать enum со всеми цветами радуги

enum Colors {
	case Red 
	case Orange 
	case Yellow 
	case Green 
	case Azule 
	case Blue 
	case Purple
}

// Создать функцию, которая содержит массив разных case'ов enum и выводит содержимое в консоль. Пример результата в консоли 'apple green', 'sun red' и т.д.

func enumFunc() {
	var enumArray = [Colors.Red, Colors.Orange, Colors.Blue]
	print(enumArray)
}

enumFunc()

// Создать функцию, которая выставляет оценки ученикам в школе, на входе принимает значение enum Score: String {<Допишите case'ы} и выводит числовое значение оценки

enum Score: String {
	case A = "A"
	case B = "B"
	case C = "C"
	case D = "D"
	case E = "E"
	case F = "F"
}

func giveScores (scores: Score) {
	switch scores {
		case .A: print(5)
		case .B: print(4)
		case .C: print(3)
		case .D: print(2)
		case .E: print(1)
		case .F: print(0)		
	}
}

giveScores(scores: Score.C)

// Создать метод, которая выводит в консоль какие автомобили стоят в гараже, используйте enum

enum Cars: String {
	case Audi = "Audi A4"
	case Bmw = "BMW X1"
	case Volkswagen = "Volkswagen B3"
	static let cars = [Audi, Bmw, Volkswagen]
}

for car in Cars.cars {
	print("\(car.rawValue)")
}